<?php

function bolgariadom_breadcrumb_setting_form() { 

  $form['breadcrumb_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Общие настройки'
  );
  
  $form['breadcrumb_settings']['bolgariadom_breadcrumb_separator'] = array(
    '#type' => 'textfield',
    '#title' => 'Разделитель ссылок',
    '#default_value' => variable_get('bolgariadom_breadcrumb_separator', ' > ')
  );
  
  $form['breadcrumb_property_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки карточки объекта'
  );
  
  $form['breadcrumb_property_settings']['bolgariadom_breadcrumb_property_type'] = array(
    '#type' => 'checkbox',
    '#title' => 'Включать в хлебные крошки тип объекта',
    '#default_value' => variable_get('bolgariadom_breadcrumb_property_type', 1)
  );
  
  $form['breadcrumb_property_settings']['bolgariadom_breadcrumb_property_location'] = array(
    '#type' => 'checkbox',
    '#title' => 'Включать в хлебные крошки расположение объекта',
    '#default_value' => variable_get('bolgariadom_breadcrumb_property_location', 1)
  );
  
  $form['breadcrumb_property_search_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Настройки списка объектов'
  );
  
  $form['breadcrumb_property_search_settings']['bolgariadom_breadcrumb_property_serch_text'] = array(
    '#type' => 'textfield',
    '#title' => 'Текст на списке недвижимости',
    '#default_value' => variable_get('bolgariadom_breadcrumb_property_serch_text', 'Поиск объектов недвижимости')
  );
  
  return system_settings_form($form);
  
}