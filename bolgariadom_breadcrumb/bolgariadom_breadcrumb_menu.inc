<?php

/** 
 * Реализация hook_permission
 */ 
function bolgariadom_breadcrumb_permission() {
  return array(
    'breadcrumb settings' => array(        
      'title' => t('Настроки бредкрамба')
    ),  
  );
}

/**
 * Implements hook_menu().
 */
function bolgariadom_breadcrumb_menu() {
  
  $items = array();
  
  $items['adminmenu/breadcrumb'] = array(
    'title' => 'Настройка Breadcrumb',  
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bolgariadom_breadcrumb_setting_form'),
    'access arguments' => array('breadcrumb settings'),
    'file' => 'bolgariadom_breadcrumb.admin.inc',
    'file path' => drupal_get_path('module', 'bolgariadom_breadcrumb') . '/includes',  
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'menu-adminmenu',
    'weight' => 22
  );
  // добавляем иконку
  pr_admin_f_api_adminmenu_icon_set('adminmenu/breadcrumb', 'fa fa-exchange');
  
  return $items;
  
}

